#运行之前保证几点:
# 1.指定下面的xianCode的数组为各个需要分解的县的code
# 2.指定下面的mdbPath为需要分解的mdb数据库路径。路径要用 \\ 或者 /
# 3.保证需要分解的mdb数据库未占用！！！！！
# 4.测试在ArcGIS 10.1下可用
# 结果会放到该mdb文件夹下,名字是:code+日期时间戳,例如440904形成的mdb就是440904201604141111.mdb
import os
import os.path
import time
import string
import arcpy
import arcpy.da
#例如 xianCode = ['440904','440905']
xianCode = ['440904'] #要用字符串形式
#mdbPath=u"H:\\E\山洪灾害\\审核汇集实施\\审核过程数据\\鹤山市\\20160420164014\\GDB.mdb"
mdbPath = u"D:/wata/拷贝.mdb"
arcpy.env.workspace = mdbPath
#遍历所有的县
for xian in xianCode:
    #首先创建该县对应的mdb
    outTableFolderPath = os.path.dirname(mdbPath)
    newMdb = xian + time.strftime("%Y%m%d%H%M%S")+".mdb"
    arcpy.CreatePersonalGDB_management(outTableFolderPath,newMdb)
    newMdbFullPath = outTableFolderPath + "/" + newMdb
    #遍历所有的Layer
    fcList = arcpy.ListFeatureClasses()
    for fc in fcList:
        desc = arcpy.Describe(fc)
        type = desc.shapeType
        print("{0}: {1}".format(fc, type))
        arcpy.CreateFeatureclass_management(newMdbFullPath,fc,type,fc,"SAME_AS_TEMPLATE","SAME_AS_TEMPLATE",desc.spatialReference)
        # Create insert cursor for table 
        InsertCursor = arcpy.InsertCursor(newMdbFullPath + "/" + fc)
        #查询出一个县的数据
        where_clause="[CADCD] = \"" + xian +"\""
        if((fc == "DANAD") or (fc == "Floodmark") or (fc == "FZCLK" )):
            where_clause = "[ADCD] like \"" + xian + "*\""
        print where_clause
        rows = arcpy.SearchCursor(mdbPath + "/" + fc,where_clause)
        # Iterate through the rows in the cursor 
        for row in rows: 
            print row.getValue("OBJECTID")
            InsertCursor.insertRow(row) 
        #清除使用到的游标
        del rows
        del InsertCursor
    #这个时候一个县就做完了
#完美收官